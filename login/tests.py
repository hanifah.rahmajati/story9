from django.test import TestCase, Client
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class UnittestStory9(TestCase) :
    def test_url_login(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'login.html')

    def test_url_proses(self):
        response = Client().get('/loginsuccess/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'proses.html')

    def test_button_dilogin(self):
        c = Client()
        response = c.get('')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content) 
        self.assertIn("Login", content)

    def test_button_dilogout(self):
        c = Client()
        response = c.get('/loginsuccess/')
        content = response.content.decode('utf8') 
        self.assertIn ("<button", content) 
        self.assertIn("Logout", content)
    

class FunctionalTestStory9(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.browser.implicitly_wait(1)
        super(FunctionalTestStory9,self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FunctionalTestStory9, self).tearDown()

    
    def test_login_dan_logut_sukses(self):
        self.browser.get('http://hanifah-rahmajati-story9.herokuapp.com/')

        uname2 = self.browser.find_element_by_id("uname")
        pw2 = self.browser.find_element_by_id("pw")
        btn2 = self.browser.find_element_by_id("btn")

        uname2.send_keys("hanifah.rahmajati")
        pw2.send_keys("testinibener")
        time.sleep(2)
        btn2.click()
        time.sleep(2)

        self.assertEqual(self.browser.current_url, "http://hanifah-rahmajati-story9.herokuapp.com/loginsuccess/")
        
        self.assertIn('hanifah.rahmajati', self.browser.page_source)
        time.sleep(2)

        logoutbtn = self.browser.find_element_by_id("out")
        logoutbtn.click()
        self.assertIn('<input', self.browser.page_source)
        self.assertIn('<button', self.browser.page_source)
        time.sleep(2)
        self.assertEqual(self.browser.current_url, "http://hanifah-rahmajati-story9.herokuapp.com/")
        time.sleep(5)
    
    def test_login_dan_logut_tidak_sukses(self):
        self.browser.get('http://hanifah-rahmajati-story9.herokuapp.com/')

        uname2 = self.browser.find_element_by_id("uname")
        pw2 = self.browser.find_element_by_id("pw")
        btn2 = self.browser.find_element_by_id("btn")

        uname2.send_keys("Test")
        time.sleep(2)
        pw2.send_keys("testinisalah")
        time.sleep(2)
        btn2.click()
        time.sleep(2)

        self.assertEqual(self.browser.current_url, "http://hanifah-rahmajati-story9.herokuapp.com/")
        self.assertIn('User not found.', self.browser.page_source)





