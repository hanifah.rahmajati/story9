from django.shortcuts import render, redirect, get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect
from django.contrib.auth.models import User
import requests

from django.contrib.auth import authenticate, login, logout
from django.urls import reverse


def loginacc(request):
    context ={}
    if request.method == "POST" :
        uname = request.POST['username']
        pw = request.POST['password']
        user = authenticate(request, username=uname, password=pw)
        if user:
            login(request, user)
            return HttpResponseRedirect(reverse('loginsuccess'))

        else:
            context["user_not_found"]= "User not found."
            return render(request, 'login.html', context)
    else:
        return render(request, 'login.html', context)

def loginsuccess(request): 
    context = {}
    context['user'] = request.user
    return render(request, 'proses.html', context)

def logoutacc(request):
    if request.method =="POST":
        logout(request)
        return HttpResponseRedirect(reverse('loginacc'))
